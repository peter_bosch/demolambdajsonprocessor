/* eslint-disable no-undef */
import { SynthUtils } from '@aws-cdk/assert';
import '@aws-cdk/assert/jest';
import * as cdk from '@aws-cdk/core';
import LambdaJsonProcessorStack from '../lib/demo-lambda-json-processor-stack';

function createStack() {
  const app = new cdk.App();
  return new LambdaJsonProcessorStack(app, 'MyTestStack');
}

// WHEN
const stack = createStack();
// THEN

test('Check snapshot', () => {
  expect(SynthUtils.toCloudFormation(stack)).toMatchSnapshot();
});

test('Check if S3 Bucket is created', () => {
  expect(stack).toHaveResource('AWS::S3::Bucket');
});

test('Check if Lambda is created', () => {
  expect(stack).toHaveResource('AWS::Lambda::Function', {});
});

test('Check if Event is triggered each minute', () => {
  expect(stack).toHaveResource('AWS::Events::Rule', {
    ScheduleExpression: 'cron(0/1 * * * ? *)',
  });
});
