/* eslint-disable no-new */
import 'source-map-support/register';
import * as cdk from '@aws-cdk/core';
import LambdaJsonProcessorStack from '../lib/demo-lambda-json-processor-stack';

require('dotenv').config();

const app = new cdk.App();

new LambdaJsonProcessorStack(app, 'LambdaJsonProcessorStack', {
  env: {
    // use environment variables pass the environment details to your application
    account: process.env.CDK_DEFAULT_ACCOUNT,
    region: process.env.CDK_DEFAULT_REGION,
  },
});
