
const AWS = require('aws-sdk');

const dynClient = new AWS.DynamoDB.DocumentClient();

const bucketName = process.env.BUCKET;
const TableName = process.env.TABLE;

const S3 = new AWS.S3();

exports.handler = async () => {
  let returnValue = '';
  // read the file from the S3 bucket
  try {
    const fileParams = { Bucket: bucketName, Key: 'demo.json' };
    const data = await S3.getObject(fileParams).promise();

    if (data && data.Body && Buffer.from(data.Body).toString('utf8') !== '') {
      const file = JSON.parse(Buffer.from(data.Body).toString('utf8'));

      // add each element to the table (could also be done bulk wise)
      Object.values(file.records).forEach(async (element) => {
        const params = {
          TableName,
          Item: {
            ...element,
          },
        };
        console.log(`adding element ${JSON.stringify(element)}`);
        await dynClient.put(params).promise();
      });

      // after adding the elements, delete the json file
      await S3.deleteObject(fileParams).promise();
      returnValue = `Added ${file.records.length} records and deleted the json file.`;
    } else {
      returnValue = 'Error: Empty file received';
    }
  } catch (error) {
    if (error.statusCode === 404) {
      returnValue = 'File not found, nothing to do';
    } else {
      returnValue = `Error occurred while reading and processing the json file: ${JSON.stringify(error)}`;
    }
  }
  console.log(returnValue);
  return returnValue;
};
