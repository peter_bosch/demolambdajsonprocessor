process.env.BUCKET = "fakebucket";

const mockS3GetObject = jest.fn();
jest.mock('aws-sdk', () => {
  return {
    S3: jest.fn(() => ({
      getObject: mockS3GetObject,
      deleteObject: jest.fn((params) => {
        return {promise() {return Promise.resolve();}}
      })
    })),
    DynamoDB: {
      DocumentClient: jest.fn(() => ({
        put: jest.fn().mockImplementation((params) => {
          return {
            promise() {
              return Promise.resolve();
            }
          };
        })
      }))
    }
  }
});

;(async () => {

  const api = require('../index.js');

  const eventTemplate = {
    httpMethod: 'GET',
    resource: '',
    path: '',
    pathParameters: {
    }
  };

  const testRecord = `
  {
    "records" : [
      {
        "pkey" : "1",
        "attrib1": "bla1",
        "attrib2": "bli1"
      }
    ]
  }`;

  test('Test no json file available', async () => {
    mockS3GetObject.mockImplementation((params) => {
      return { promise() { return Promise.reject({ statusCode: 404 }); } };
    });

    const response = await api.handler(eventTemplate);
    expect(response).toBe('File not found, nothing to do');
  });

  test('Test adding 1 record from a json file', async() => {
    mockS3GetObject.mockImplementation((params) => {
      return {
        promise() {
          let data = {
            Body: Buffer.from(testRecord, 'utf-8')
          }
          return Promise.resolve(data);
        }
      };
    });
    const response = await api.handler(eventTemplate);
    expect(response).toBe('Added 1 records and deleted the json file.');
  });

  test('Test using a not existing bucketname', async () => {
    mockS3GetObject.mockImplementation((params) => {
      return { promise() { return Promise.reject({ statusCode: 403 }); } };
    });

    const response = await api.handler(eventTemplate);
    expect(response).toContain('Error occurred while reading and processing the json file: ');
  });

  test('Test receiving an empty file', async() => {
    mockS3GetObject.mockImplementation((params) => {
      return {
        promise() {
          let data = { 
            Body: Buffer.from('', 'utf-8')
          }
          return Promise.resolve(data);
        }
      };
    });
    const response = await api.handler(eventTemplate);
    expect(response).toBe('Error: Empty file received');
  });


})();

