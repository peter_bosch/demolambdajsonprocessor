// For a detailed explanation regarding each configuration property, visit:
// https://jestjs.io/docs/en/configuration.html
module.exports = {
  roots: ['<rootDir>/__tests__','<rootDir>/resources'],
  testMatch: ['<rootDir>/resources/**/*.test.js', '<rootDir>/__tests__/**/*.test.ts'],
  transform: {
    '^.+\\.tsx?$': 'ts-jest'
  },
  collectCoverage: true,
  setupFilesAfterEnv: ['<rootDir>/__tests__/setup.js'],
  verbose: true,
};
