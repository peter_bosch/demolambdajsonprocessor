# demo lambda json processor 

This package shows a setup for using the cdk for setting up a Lambda that looks for a json file in a S3 bucket. 
When the json file exists it will update a DynamoDB with the content of each record. 

The infrastructure is setup via CDK, it will create an S3 bucket, Lambda, DynamoDB table and an Event that triggers the Lambda each minute. 

Using jest as test framework both the CDK code and the Lambda code can be tested by running `npm test`
As a snapshot is created to prevent unnoticed changes, you can update it with `npm test -- -u`

A CI/CD Pipeline is setup for building, testing, diff, deploy and destroy. Note after the diff stage start the deploy manually so the diff can be checked first.
