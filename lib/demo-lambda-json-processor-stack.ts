import * as cdk from '@aws-cdk/core';
import * as S3 from '@aws-cdk/aws-s3';
import * as Lambda from '@aws-cdk/aws-lambda';
import * as Lambdajs from '@aws-cdk/aws-lambda-nodejs';
import * as Event from '@aws-cdk/aws-events';
import * as Tasks from '@aws-cdk/aws-events-targets';
import * as Dynamodb from '@aws-cdk/aws-dynamodb';

export default class LambdaJsonProcessorStack extends cdk.Stack {
  schedule: Event.Rule;

  constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    // The code that defines your stack goes here
    const bucket = new S3.Bucket(this, 'myBucket', {
      removalPolicy: cdk.RemovalPolicy.DESTROY,
    });

    const table = new Dynamodb.Table(this, 'myDynamodb', {
      partitionKey: {
        name: 'pkey',
        type: Dynamodb.AttributeType.STRING,
      },
      removalPolicy: cdk.RemovalPolicy.DESTROY,
    });

    const jsonProcessor = new Lambdajs.NodejsFunction(this, 'myBucketReader', {
      runtime: Lambda.Runtime.NODEJS_12_X,
      entry: 'resources/jsonProcessor/index.js',
      handler: 'handler',
      environment: {
        BUCKET: bucket.bucketName,
        TABLE: table.tableName,
      },
    });

    bucket.grantReadWrite(jsonProcessor);
    bucket.grantDelete(jsonProcessor);
    table.grantReadWriteData(jsonProcessor);

    const processorTask = new Tasks.LambdaFunction(jsonProcessor);
    const schedule = new Event.Rule(this, 'myRule', {
      schedule: Event.Schedule.cron({ minute: '0/1' }),
      targets: [processorTask],
    });
    this.schedule = schedule;
  }
}
